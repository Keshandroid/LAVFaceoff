package com.example.lav_faceoff

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.MediaPlayer
import android.os.*
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.lav_faceoff.databinding.ActivityStartBinding
import com.example.lav_faceoff.utils.StoreUserData
import com.example.lav_record.utils.Constants


class StartActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityStartBinding
    var mp: MediaPlayer? = null
    var secondsOfWistle = 0
    var data: String? = null
    var flag = false
    val handler: Handler = Handler(Looper.getMainLooper())


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStartBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setListener()

        val storedata = StoreUserData(this)
        data = storedata.getString(Constants.secondsOfWistle)

        try {
            secondsOfWistle = data?.toInt()!!
            secondsOfWistle *= 1000
        } catch (e: NumberFormatException) {
            Log.d("numberFormatException", e.message.toString())
        }


    }

    private fun handleStartTimer(view: View?) {
        val intent = Intent(this, BroadcastService::class.java)
        mp = MediaPlayer.create(this, R.raw.whistle)
        binding.textCity.visibility = View.VISIBLE

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            intent.putExtra("inputExtra", "inputExtra")
            ContextCompat.startForegroundService(this, intent)
            startService(intent)


            if (Constants.flag) {
                object : CountDownTimer(secondsOfWistle.toLong(), 1000) {
                    override fun onTick(millisUntilFinished: Long) {
                        binding.textCity.text = (millisUntilFinished / 1000).toString()
                        binding.imgCity.visibility = View.GONE
                    }

                    override fun onFinish() {

                        flag = false

                        binding.textCity.text = "done!"
                        binding.imgCity.visibility = View.VISIBLE
                        try {
                            mp?.start()

                        } catch (e: IllegalStateException) {
                            Log.d("ILLEGALSTATEEXCEPTION", e.message.toString())
                        }
                        start()


                    }
                }.start()


            } else {
                // flag = true
                object : CountDownTimer(5000, 1000) {
                    override fun onTick(millisUntilFinished: Long) {
                        binding.textCity.text = (millisUntilFinished / 1000).toString()
                        binding.imgCity.visibility = View.GONE
                    }

                    @SuppressLint("SetTextI18n")
                    override fun onFinish() {
                        binding.textCity.text = "done!"
                        binding.imgCity.visibility = View.VISIBLE

                        try {
                            mp?.start()
                        } catch (e: IllegalStateException) {
                            Log.d("ILLEGALSTATEEXCEPTION", e.message.toString())
                        }
                        start()

                        //  binding.imgCity.visibility = View.VISIBLE

                    }
                }.start()

            }


        }

    }

    private fun handleCancelTimer(view: View?) {
        //  if (mp != null) {
        mp?.stop()
        mp?.release()
        // }
        val intent = Intent(this, BroadcastService::class.java)
        intent.putExtra("countdownTimerFinished", "inputExtra")
        stopService(intent)

        Toast.makeText(this, "Timer Stopped", Toast.LENGTH_LONG).show()
        Log.i(TAG, "TIMER STOPPED")
        binding.textCity.text = "Stopped"
        finishAffinity()

        // onStop()


    }

    /* CountDown */
    private val broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            updateGUI(intent)
        }
    }


    override fun onResume() {

        super.onResume()
        registerReceiver(broadcastReceiver, IntentFilter(BroadcastService.COUNTDOWN_BR))
        Log.i(TAG, "Registered broadcast receiver")
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(broadcastReceiver)
        Log.i(TAG, "Unregistered broadcast receiver")
    }

    override fun onStop() {
        try {
            unregisterReceiver(broadcastReceiver)
            //updateGUI(intent)
        } catch (e: Exception) {
            // Receiver was probably already stopped in onPause()
        }
        super.onStop()
    }

    @SuppressLint("SetTextI18n")
    private fun updateGUI(intent: Intent?) {
        if (intent!!.extras != null) {
            val millisUntilFinished = intent.getLongExtra("countdown", 0)
            val seconds = millisUntilFinished / 100 % 6
            val minutes = millisUntilFinished / (100 * 6) % 6
            val hours = millisUntilFinished / (100 * 6 * 6) % 6
            val time = "$hours : $minutes : $seconds"
            Log.d("numbertext", secondsOfWistle.toString())
            val countdownTimerRunning = intent.getBooleanExtra("countdownTimerRunning", false)
            //  tvTimerRunningState = findViewById<View>(android.R.id.tvTimerRunningState)
            if (countdownTimerRunning) {
                //   binding.textCity.text = "CountdownTimerRunning"
            } else {
                // binding.textCity.text = "0 : 0 : 0"
                //  binding.textCity.text = "CountdownTimerNotRunning"
            }
            val countdownTimerFinished = intent.getBooleanExtra("countdownTimerFinished", false)
            //  tvTimerFinishedState = findViewById<View>(android.R.id.tvTimerFinishedState)
            if (countdownTimerFinished) {
                // binding.textCity.text = "Finished"
            } else {
                // binding.textCity.text = "Finished"
            }
        }
    }


    private fun setListener() {
        binding.btnSettings.setOnClickListener(this)
        binding.btnStart.setOnClickListener(this)
        binding.btnStop.setOnClickListener(this)
    }


    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btn_settings -> {
                val intent = Intent(this@StartActivity, SettingsActivity::class.java)
                startActivity(intent)
                finishAffinity()
            }

            R.id.btnStart -> {

                handleStartTimer(view)

                binding.rlStart.visibility = View.GONE
                binding.rlStop.visibility = View.VISIBLE
                binding.rlWhistle.visibility = View.VISIBLE
                binding.btnSettings.visibility = View.GONE
                //  binding.textCity.visibility = View.VISIBLE

                /*    val intent = Intent(
                        this@StartActivity, StopActivity::class.java)
                    startActivity(intent)*/
            }

            R.id.btnStop -> {
                handleCancelTimer(view)
                /*  if (mp != null) {
                      mp?.stop()
                      mp?.release()
                  }*/
                //binding.imgCity.visibility = View.VISIBLE
                //   binding.textCity.visibility = View.VISIBLE
                binding.btnSettings.visibility = View.GONE
            }

        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }
}

