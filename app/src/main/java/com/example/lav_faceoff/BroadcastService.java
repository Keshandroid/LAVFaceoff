package com.example.lav_faceoff;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.example.lav_faceoff.utils.StoreUserData;
import com.example.lav_record.utils.Constants;

public class BroadcastService extends Service {

    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    private final static String TAG = "BroadcastService";
    public static final String COUNTDOWN_BR = "com.example.lav_faceOff";
    Intent bi = new Intent(COUNTDOWN_BR);
    int secondsOfWistle;
    CountDownTimer cdt = null;


    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "Starting timer...");
        StoreUserData storedata = new StoreUserData(this);
        String data = storedata.getString(Constants.INSTANCE.getSecondsOfWistle());

        try {
            secondsOfWistle = Integer.parseInt(data);
            secondsOfWistle = secondsOfWistle * 1000;
        } catch (NumberFormatException e) {
            Log.d("NumberFormatException", e.getLocalizedMessage());
        }


        Log.d("secondsOfWistle", secondsOfWistle + "");
        cdt = new CountDownTimer(secondsOfWistle, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                Log.i(TAG, "Countdown seconds remaining: " + millisUntilFinished / 1000);
                bi.putExtra("countdown", millisUntilFinished);
                bi.putExtra("countdownTimerRunning", false);
                bi.putExtra("countdownTimerFinished", false);
                sendBroadcast(bi);
            }

            @Override
            public void onFinish() {
                Log.i(TAG, "Timer finished");
                bi.putExtra("countdownTimerFinished", false);
                sendBroadcast(bi);
                stopForeground(true);
                stopSelf();
            }
        };
        cdt.start();

    }

    @Override
    public void onDestroy() {
        cdt.cancel();
        Log.i(TAG, "Timer cancelled");
        bi.putExtra("countdownTimerRunning", false);
        sendBroadcast(bi);
        super.onDestroy();
    }

    @RequiresApi(api = Build.VERSION_CODES.S)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        /* Notification */
        String input = intent.getStringExtra("inputExtra");
        createNotificationChannel();
        Intent notificationIntent = new Intent(this, BroadcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_MUTABLE);
        /* NotificationBuilder */
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID).setContentTitle("Foreground Service").setContentText(input).setSmallIcon(R.drawable.ic_launcher_background).setContentIntent(pendingIntent).build();
        startForeground(1, notification);
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(CHANNEL_ID, "Foreground Service Channel", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }
}
