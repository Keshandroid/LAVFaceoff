package com.example.lav_faceoff

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.app.AppCompatActivity
import com.example.lav_faceoff.databinding.ActivitySettingsBinding
import com.example.lav_faceoff.utils.StoreUserData
import com.example.lav_record.utils.Constants
import com.example.lav_record.utils.Constants.flag


class SettingsActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivitySettingsBinding

    var minteger = 0
    private var volumeSeekbar: SeekBar? = null
    private var audioManager: AudioManager? = null
    //var INSTANCE: SettingsActivity? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //  INSTANCE=this

        setListener()
        initControls()

    }

    private fun increaseInteger(view: View?) {
         minteger += 1
         display(minteger)
     }

    private fun decreaseInteger(view: View?) {
        minteger -= 1
        display(minteger)
    }

    @SuppressLint("SetTextI18n")
    fun display(number: Int) {
        binding.txtNumber.text = "" + number
        // Log.d("number", number.toString())
        var numstore = number
        //var numStore = binding.txtNumber
        intent.putExtra("number", numstore)
        Log.d("number", numstore.toString())

        val storedata = StoreUserData(this)
        storedata.setString(Constants.secondsOfWistle, numstore.toString())

        flag = true


    }


    private fun initControls() {
        try {
            audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
            binding.seekbar.max = audioManager!!.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
            binding.seekbar.progress = audioManager!!.getStreamVolume(AudioManager.STREAM_MUSIC)
            binding.seekbar.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
                override fun onStopTrackingTouch(arg0: SeekBar) {

                }

                override fun onStartTrackingTouch(arg0: SeekBar) {

                }

                override fun onProgressChanged(arg0: SeekBar, progress: Int, arg2: Boolean) {
                    audioManager!!.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0)
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setListener() {
        binding.back.setOnClickListener(this)
        binding.txtNumber.setOnClickListener(this)
        binding.dropUp.setOnClickListener(this)
        binding.dropdown.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {

            R.id.back -> {
                val i = Intent(this, StartActivity::class.java)
                startActivity(i)
                finishAffinity()
            }

            R.id.dropUp -> {
                increaseInteger(view)
            }
            R.id.dropdown -> {
                decreaseInteger(view)

            }

        }

    }


}
